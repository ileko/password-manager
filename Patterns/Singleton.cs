﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Password_Manager
{
    public sealed class Singleton
    {
        private static Singleton instance = null;
        private static readonly object padlock = new object();

        Singleton()
        {
        }

        public static Singleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Singleton();
                    }
                    return instance;
                }
            }
        }

        // Return user data for logged user
        public List<Record> GetUserData(int UserId, string Hash)
        {
            try
            {
                var context = new RecordEntities();
                var validation = context.Records.Where(e => e.UserId == UserId);
                var result = validation.ToList();

                List<Record> records = new List<Record>();
                foreach (var item in result)
                {
                    string password = DecryptText(item.Password, Hash);
                    records.Add(new Record() { Description = item.Description, Email = item.Email, Password = password });
                }

                return records;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

        }

        // Decrypt user passwords using main password like hash
        public string DecryptText(string enctext, string hash)
        {
            Console.WriteLine("Encrypted password: " + enctext);
            byte[] data = Convert.FromBase64String(enctext);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    enctext = UTF8Encoding.UTF8.GetString(results);
                    Console.WriteLine("Decrypted password: " + enctext);
                    return enctext;
                }
            }
        }

        // Encrypt user password input using hash
        public string EncryptText(string text, string hash)
        {
            Console.WriteLine("PasswordToBeEncrypted: " + text);
            byte[] data = UTF8Encoding.UTF8.GetBytes(text);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripDes.CreateEncryptor();
                    byte[] results = transform.TransformFinalBlock(data, 0, data.Length);
                    text = Convert.ToBase64String(results, 0, results.Length);
                    return text;
                }
            }
        }

        // check user login data
        public int validation(string username, string password)
        {
            try
            {
                // validation in database | Entity & Linq
                var context = new UsersEntities();
                var validation = context.Users.Where(e => e.Email == username);
                var result = validation.FirstOrDefault();

                // converting user input to hash using SHA256
                var passwordBytes = Encoding.ASCII.GetBytes(password);
                var sha = new SHA256Managed();
                var hash = sha.ComputeHash(passwordBytes);

                // converting hash from byte array to string
                StringBuilder stringBuilder = new StringBuilder();
                foreach (byte b in hash)
                {
                    stringBuilder.AppendFormat("{0:x2}", b);
                }
                string hashString = stringBuilder.ToString();

                if(result.Password == null)
                {
                    return 0;
                }

                // compare user input & database data | USER VALIDATION
                if (object.Equals(result.Password, hashString))
                {
                    // return logged user Id
                    return result.Id;
                }

                return 0;
            }
            catch (LoginFailed e)
            {
                Console.WriteLine("Exception: " + e.Message);
                throw;
            }
        }

        // reading user data from local storage (json format)
        public List<Record> LoadFromJsonFile()
        {
            using (StreamReader r = new StreamReader(@"C:\data.json"))
            {
                string json = r.ReadToEnd();
                List<Record> records = JsonConvert.DeserializeObject<List<Record>>(json);

                return records;
            }
        }
    }
}
