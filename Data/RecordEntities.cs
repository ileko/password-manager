﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Password_Manager
{
    public partial class RecordEntities : DbContext
    {
        public RecordEntities()
            : base("name=RecordEntities")
        {
        }

        public virtual DbSet<Record> Records { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Record>()
                .Property(e => e.UserId);

            modelBuilder.Entity<Record>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Record>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Record>()
                .Property(e => e.Description)
                .IsUnicode(false);
        }
    }
}