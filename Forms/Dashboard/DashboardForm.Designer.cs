﻿namespace Password_Manager
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashboardForm));
            this.SideMenuPanel = new System.Windows.Forms.Panel();
            this.btnExit = new FontAwesome.Sharp.IconButton();
            this.btnLocalData = new FontAwesome.Sharp.IconButton();
            this.btnGenerate = new FontAwesome.Sharp.IconButton();
            this.btnAddPassword = new FontAwesome.Sharp.IconButton();
            this.btnPasswords = new FontAwesome.Sharp.IconButton();
            this.LogoPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelContent = new System.Windows.Forms.Panel();
            this.SideMenuPanel.SuspendLayout();
            this.LogoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SideMenuPanel
            // 
            this.SideMenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(33)))), ((int)(((byte)(89)))));
            this.SideMenuPanel.Controls.Add(this.btnExit);
            this.SideMenuPanel.Controls.Add(this.btnLocalData);
            this.SideMenuPanel.Controls.Add(this.btnGenerate);
            this.SideMenuPanel.Controls.Add(this.btnAddPassword);
            this.SideMenuPanel.Controls.Add(this.btnPasswords);
            this.SideMenuPanel.Controls.Add(this.LogoPanel);
            this.SideMenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SideMenuPanel.Location = new System.Drawing.Point(0, 0);
            this.SideMenuPanel.Name = "SideMenuPanel";
            this.SideMenuPanel.Size = new System.Drawing.Size(214, 500);
            this.SideMenuPanel.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.IconChar = FontAwesome.Sharp.IconChar.SignOutAlt;
            this.btnExit.IconColor = System.Drawing.Color.White;
            this.btnExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnExit.IconSize = 32;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(0, 391);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnExit.Size = new System.Drawing.Size(214, 60);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLocalData
            // 
            this.btnLocalData.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLocalData.FlatAppearance.BorderSize = 0;
            this.btnLocalData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocalData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocalData.ForeColor = System.Drawing.Color.White;
            this.btnLocalData.IconChar = FontAwesome.Sharp.IconChar.Download;
            this.btnLocalData.IconColor = System.Drawing.Color.White;
            this.btnLocalData.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnLocalData.IconSize = 32;
            this.btnLocalData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLocalData.Location = new System.Drawing.Point(0, 331);
            this.btnLocalData.Name = "btnLocalData";
            this.btnLocalData.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnLocalData.Size = new System.Drawing.Size(214, 60);
            this.btnLocalData.TabIndex = 5;
            this.btnLocalData.Text = "Local data";
            this.btnLocalData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLocalData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLocalData.UseVisualStyleBackColor = true;
            this.btnLocalData.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGenerate.FlatAppearance.BorderSize = 0;
            this.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.ForeColor = System.Drawing.Color.White;
            this.btnGenerate.IconChar = FontAwesome.Sharp.IconChar.Screwdriver;
            this.btnGenerate.IconColor = System.Drawing.Color.White;
            this.btnGenerate.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnGenerate.IconSize = 32;
            this.btnGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerate.Location = new System.Drawing.Point(0, 271);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnGenerate.Size = new System.Drawing.Size(214, 60);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenerate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnAddPassword
            // 
            this.btnAddPassword.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddPassword.FlatAppearance.BorderSize = 0;
            this.btnAddPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPassword.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPassword.ForeColor = System.Drawing.Color.White;
            this.btnAddPassword.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.btnAddPassword.IconColor = System.Drawing.Color.White;
            this.btnAddPassword.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnAddPassword.IconSize = 32;
            this.btnAddPassword.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddPassword.Location = new System.Drawing.Point(0, 211);
            this.btnAddPassword.Name = "btnAddPassword";
            this.btnAddPassword.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnAddPassword.Size = new System.Drawing.Size(214, 60);
            this.btnAddPassword.TabIndex = 3;
            this.btnAddPassword.Text = "Add password";
            this.btnAddPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddPassword.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddPassword.UseVisualStyleBackColor = true;
            this.btnAddPassword.Click += new System.EventHandler(this.btnAddPassword_Click);
            // 
            // btnPasswords
            // 
            this.btnPasswords.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPasswords.FlatAppearance.BorderSize = 0;
            this.btnPasswords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPasswords.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasswords.ForeColor = System.Drawing.Color.White;
            this.btnPasswords.IconChar = FontAwesome.Sharp.IconChar.Lock;
            this.btnPasswords.IconColor = System.Drawing.Color.White;
            this.btnPasswords.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnPasswords.IconSize = 32;
            this.btnPasswords.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPasswords.Location = new System.Drawing.Point(0, 151);
            this.btnPasswords.Name = "btnPasswords";
            this.btnPasswords.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.btnPasswords.Size = new System.Drawing.Size(214, 60);
            this.btnPasswords.TabIndex = 2;
            this.btnPasswords.Text = "Passwords";
            this.btnPasswords.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPasswords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPasswords.UseVisualStyleBackColor = true;
            this.btnPasswords.Click += new System.EventHandler(this.btnPasswords_Click);
            // 
            // LogoPanel
            // 
            this.LogoPanel.Controls.Add(this.pictureBox1);
            this.LogoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogoPanel.Location = new System.Drawing.Point(0, 0);
            this.LogoPanel.Name = "LogoPanel";
            this.LogoPanel.Size = new System.Drawing.Size(214, 151);
            this.LogoPanel.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(36, 32);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(147, 99);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(214, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(606, 36);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Keep Calm Med", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(573, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panelContent
            // 
            this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContent.Location = new System.Drawing.Point(214, 36);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(606, 464);
            this.panelContent.TabIndex = 2;
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 500);
            this.Controls.Add(this.panelContent);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.SideMenuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DashboardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VUB Tool";
            this.Load += new System.EventHandler(this.DashboardForm_Load);
            this.SideMenuPanel.ResumeLayout(false);
            this.LogoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SideMenuPanel;
        private System.Windows.Forms.Panel LogoPanel;
        private FontAwesome.Sharp.IconButton btnExit;
        private FontAwesome.Sharp.IconButton btnLocalData;
        private FontAwesome.Sharp.IconButton btnGenerate;
        private FontAwesome.Sharp.IconButton btnAddPassword;
        private FontAwesome.Sharp.IconButton btnPasswords;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelContent;
        private System.Windows.Forms.Label label2;
    }
}

