﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;

namespace Password_Manager
{
    public partial class DashboardForm : Form
    {
        private IconButton currentBtn;
        private Panel leftBorderBtn;
        private Form activeForm;
        public int UserId { get; set; }
        public string Password { get; set; }
        public DashboardForm(int UserId, string Password)
        {
            InitializeComponent();
            this.UserId = UserId;
            this.Password = Password;

            // side menu settings
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(5, 60);
            SideMenuPanel.Controls.Add(leftBorderBtn);
        }

        // color structure
        private struct RGBColors
        {
            public static Color color1 = Color.FromArgb(255, 255, 255);
        }

        // set active style to button
        private void ActivateButton(object senderBtn, Color color)
        {
            if (senderBtn != null)
            {
                DisableButton();
                //Button
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(69, 107, 206);
                currentBtn.ForeColor = color;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = color;
                currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;
                //Left border button
                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0, currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
            }
        }

        // set disabled style to button
        private void DisableButton()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(13, 33, 89);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.White;
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        // getting data for moving window feature
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int IParam);

        // switching between forms
        private void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            ActivateButton(btnSender, RGBColors.color1);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelContent.Controls.Add(childForm);
            this.panelContent.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }


        // SIDE MENU BUTTONS EVENTS
        private void btnPasswords_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.Passwords(UserId, Password), sender);
        }

        private void btnAddPassword_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.AddPassword(UserId, Password), sender);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.Generate(), sender);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.LocalData(), sender);
        }

        // moving window feature
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        // set first tab
        private void DashboardForm_Load(object sender, EventArgs e)
        {
            btnPasswords.PerformClick();
        }

        // EXIT APPLICATION
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
