﻿namespace Password_Manager.Forms
{
    partial class Generate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerate = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tbPasswordLength = new System.Windows.Forms.TextBox();
            this.tfPassowrdLength = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblGeneratedPassword = new System.Windows.Forms.Label();
            this.btnCopyToClipboard = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.AutoSize = true;
            this.btnGenerate.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGenerate.Depth = 0;
            this.btnGenerate.Icon = null;
            this.btnGenerate.Location = new System.Drawing.Point(254, 189);
            this.btnGenerate.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Primary = true;
            this.btnGenerate.Size = new System.Drawing.Size(89, 36);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // tbPasswordLength
            // 
            this.tbPasswordLength.Location = new System.Drawing.Point(250, 157);
            this.tbPasswordLength.Name = "tbPasswordLength";
            this.tbPasswordLength.Size = new System.Drawing.Size(100, 20);
            this.tbPasswordLength.TabIndex = 1;
            // 
            // tfPassowrdLength
            // 
            this.tfPassowrdLength.Depth = 0;
            this.tfPassowrdLength.Hint = "";
            this.tfPassowrdLength.Location = new System.Drawing.Point(245, 128);
            this.tfPassowrdLength.MaxLength = 32767;
            this.tfPassowrdLength.MouseState = MaterialSkin.MouseState.HOVER;
            this.tfPassowrdLength.Name = "tfPassowrdLength";
            this.tfPassowrdLength.PasswordChar = '\0';
            this.tfPassowrdLength.SelectedText = "";
            this.tfPassowrdLength.SelectionLength = 0;
            this.tfPassowrdLength.SelectionStart = 0;
            this.tfPassowrdLength.Size = new System.Drawing.Size(118, 23);
            this.tfPassowrdLength.TabIndex = 2;
            this.tfPassowrdLength.TabStop = false;
            this.tfPassowrdLength.Text = "Password length";
            this.tfPassowrdLength.UseSystemPasswordChar = false;
            // 
            // lblGeneratedPassword
            // 
            this.lblGeneratedPassword.AutoSize = true;
            this.lblGeneratedPassword.Location = new System.Drawing.Point(211, 277);
            this.lblGeneratedPassword.Name = "lblGeneratedPassword";
            this.lblGeneratedPassword.Size = new System.Drawing.Size(182, 13);
            this.lblGeneratedPassword.TabIndex = 3;
            this.lblGeneratedPassword.Text = "Generated passwod will appear here.";
            // 
            // btnCopyToClipboard
            // 
            this.btnCopyToClipboard.AutoSize = true;
            this.btnCopyToClipboard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCopyToClipboard.Depth = 0;
            this.btnCopyToClipboard.Icon = null;
            this.btnCopyToClipboard.Location = new System.Drawing.Point(224, 334);
            this.btnCopyToClipboard.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCopyToClipboard.Name = "btnCopyToClipboard";
            this.btnCopyToClipboard.Primary = true;
            this.btnCopyToClipboard.Size = new System.Drawing.Size(155, 36);
            this.btnCopyToClipboard.TabIndex = 4;
            this.btnCopyToClipboard.Text = "Copy to clipboard";
            this.btnCopyToClipboard.UseVisualStyleBackColor = true;
            this.btnCopyToClipboard.Click += new System.EventHandler(this.btnCopyToClipboard_Click);
            // 
            // Generate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 464);
            this.Controls.Add(this.btnCopyToClipboard);
            this.Controls.Add(this.lblGeneratedPassword);
            this.Controls.Add(this.tfPassowrdLength);
            this.Controls.Add(this.tbPasswordLength);
            this.Controls.Add(this.btnGenerate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Generate";
            this.Text = "Generate";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialRaisedButton btnGenerate;
        private System.Windows.Forms.TextBox tbPasswordLength;
        private MaterialSkin.Controls.MaterialSingleLineTextField tfPassowrdLength;
        private System.Windows.Forms.Label lblGeneratedPassword;
        private MaterialSkin.Controls.MaterialRaisedButton btnCopyToClipboard;
    }
}