﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IdentityModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Manager.Forms
{
    public partial class AddPassword : Form
    {
        public int UserId { get; }
        private string Password { get; }
        private Singleton singleton;

        public AddPassword(int UserId, string Password)
        {
            InitializeComponent();
            this.UserId = UserId;
            this.Password = Password;
            this.singleton = Singleton.Instance;

            Console.WriteLine("ID: " + UserId);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string encryptedPassword = singleton.EncryptText(tfPassword.Text, Password);
            try
            {
                var context = new RecordEntities();
                var passwordData = new Record() { UserId = UserId, Email = tfEmail.Text, Password = encryptedPassword, Description = tfDescription.Text };
                context.Records.Add(passwordData);
                context.SaveChanges();
            }
            catch(Exception ex)
            {
                Console.WriteLine("PORUKA " + ex.Message);
            }
        }
    }

}
