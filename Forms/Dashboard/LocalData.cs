﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Manager.Forms
{
    public partial class LocalData : Form
    {
        private Singleton singleton;
        public LocalData()
        {
            InitializeComponent();
            singleton = Singleton.Instance;
        }

        // load json data from local storage (json format)
        public void LoadJson()
        {
            List<Record> records = singleton.LoadFromJsonFile();
            foreach (var record in records)
            {
                lbJsonData.Items.Add(String.Format("Email: {0}, Password: {1}", record.Email, record.Password));
            }
        }

        private void LocalData_Load(object sender, EventArgs e)
        {
            LoadJson();
        }
    }
}
