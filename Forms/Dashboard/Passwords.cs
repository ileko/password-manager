﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using Newtonsoft.Json;

namespace Password_Manager.Forms
{
    public partial class Passwords : Form
    {
        Thread jsonDretva;
        public Singleton singleton;
        public int UserId { get; }
        public string Hash { get; }

        public Passwords(int UserId, string hash)
        {
            InitializeComponent();

            this.UserId = UserId;
            this.Hash = hash;
            this.singleton = Singleton.Instance;

            jsonDretva = new Thread(new ThreadStart(SaveUserDataToJson));
        }

        private void Passwords_Load(object sender, EventArgs e)
        {
            jsonDretva.Start();
            LoadUserData();
        }

        // Load user data in DataGridView
        private void LoadUserData()
        {
            List<Record> records = singleton.GetUserData(UserId, Hash);

            foreach (var record in records)
            {
                passwordDataBindingSource.Add(new Record() { Description = record.Description, Email = record.Email, Password = record.Password });
            }
        }

        // Save user data to local storage in json format
        private void SaveUserDataToJson()
        {
            List<Record> records = singleton.GetUserData(UserId, Hash);

            try
            {
                string json = JsonConvert.SerializeObject(records.ToArray());
                System.IO.File.WriteAllText(@"C:\data.json", json);
            }
            catch (WritingToFileFailed e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        // Copy button in DataGridView - copy to clipboard feature
        private void dgvPasswords_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvPasswords.Columns[e.ColumnIndex].Name == "Copy")
            {
                var data = (Record)passwordDataBindingSource.Current;
                Clipboard.SetText(data.Password);
            }
        }
    }
}
