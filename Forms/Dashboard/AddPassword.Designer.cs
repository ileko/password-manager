﻿namespace Password_Manager.Forms
{
    partial class AddPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tfEmail = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tfPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tfDescription = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnSave = new MaterialSkin.Controls.MaterialRaisedButton();
            this.iconPictureBox1 = new FontAwesome.Sharp.IconPictureBox();
            this.iconPictureBox2 = new FontAwesome.Sharp.IconPictureBox();
            this.iconPictureBox3 = new FontAwesome.Sharp.IconPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tfEmail
            // 
            this.tfEmail.Depth = 0;
            this.tfEmail.Hint = "";
            this.tfEmail.Location = new System.Drawing.Point(233, 106);
            this.tfEmail.MaxLength = 32767;
            this.tfEmail.MouseState = MaterialSkin.MouseState.HOVER;
            this.tfEmail.Name = "tfEmail";
            this.tfEmail.PasswordChar = '\0';
            this.tfEmail.SelectedText = "";
            this.tfEmail.SelectionLength = 0;
            this.tfEmail.SelectionStart = 0;
            this.tfEmail.Size = new System.Drawing.Size(144, 23);
            this.tfEmail.TabIndex = 3;
            this.tfEmail.TabStop = false;
            this.tfEmail.Text = "Email";
            this.tfEmail.UseSystemPasswordChar = false;
            // 
            // tfPassword
            // 
            this.tfPassword.Depth = 0;
            this.tfPassword.Hint = "";
            this.tfPassword.Location = new System.Drawing.Point(233, 163);
            this.tfPassword.MaxLength = 32767;
            this.tfPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.tfPassword.Name = "tfPassword";
            this.tfPassword.PasswordChar = '\0';
            this.tfPassword.SelectedText = "";
            this.tfPassword.SelectionLength = 0;
            this.tfPassword.SelectionStart = 0;
            this.tfPassword.Size = new System.Drawing.Size(144, 23);
            this.tfPassword.TabIndex = 4;
            this.tfPassword.TabStop = false;
            this.tfPassword.Text = "Password";
            this.tfPassword.UseSystemPasswordChar = false;
            // 
            // tfDescription
            // 
            this.tfDescription.Depth = 0;
            this.tfDescription.Hint = "";
            this.tfDescription.Location = new System.Drawing.Point(233, 217);
            this.tfDescription.MaxLength = 32767;
            this.tfDescription.MouseState = MaterialSkin.MouseState.HOVER;
            this.tfDescription.Name = "tfDescription";
            this.tfDescription.PasswordChar = '\0';
            this.tfDescription.SelectedText = "";
            this.tfDescription.SelectionLength = 0;
            this.tfDescription.SelectionStart = 0;
            this.tfDescription.Size = new System.Drawing.Size(144, 23);
            this.tfDescription.TabIndex = 5;
            this.tfDescription.TabStop = false;
            this.tfDescription.Text = "Description";
            this.tfDescription.UseSystemPasswordChar = false;
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSave.Depth = 0;
            this.btnSave.Icon = null;
            this.btnSave.Location = new System.Drawing.Point(273, 270);
            this.btnSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSave.Name = "btnSave";
            this.btnSave.Primary = true;
            this.btnSave.Size = new System.Drawing.Size(55, 36);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // iconPictureBox1
            // 
            this.iconPictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.iconPictureBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox1.IconChar = FontAwesome.Sharp.IconChar.MailBulk;
            this.iconPictureBox1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconPictureBox1.IconSize = 28;
            this.iconPictureBox1.Location = new System.Drawing.Point(198, 101);
            this.iconPictureBox1.Name = "iconPictureBox1";
            this.iconPictureBox1.Size = new System.Drawing.Size(29, 28);
            this.iconPictureBox1.TabIndex = 7;
            this.iconPictureBox1.TabStop = false;
            // 
            // iconPictureBox2
            // 
            this.iconPictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.iconPictureBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox2.IconChar = FontAwesome.Sharp.IconChar.Lock;
            this.iconPictureBox2.IconColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconPictureBox2.IconSize = 28;
            this.iconPictureBox2.Location = new System.Drawing.Point(198, 158);
            this.iconPictureBox2.Name = "iconPictureBox2";
            this.iconPictureBox2.Size = new System.Drawing.Size(29, 28);
            this.iconPictureBox2.TabIndex = 8;
            this.iconPictureBox2.TabStop = false;
            // 
            // iconPictureBox3
            // 
            this.iconPictureBox3.BackColor = System.Drawing.SystemColors.Control;
            this.iconPictureBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox3.IconChar = FontAwesome.Sharp.IconChar.StickyNote;
            this.iconPictureBox3.IconColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox3.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconPictureBox3.IconSize = 28;
            this.iconPictureBox3.Location = new System.Drawing.Point(198, 212);
            this.iconPictureBox3.Name = "iconPictureBox3";
            this.iconPictureBox3.Size = new System.Drawing.Size(29, 28);
            this.iconPictureBox3.TabIndex = 9;
            this.iconPictureBox3.TabStop = false;
            // 
            // AddPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 464);
            this.Controls.Add(this.iconPictureBox3);
            this.Controls.Add(this.iconPictureBox2);
            this.Controls.Add(this.iconPictureBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tfDescription);
            this.Controls.Add(this.tfPassword);
            this.Controls.Add(this.tfEmail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddPassword";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialSingleLineTextField tfEmail;
        private MaterialSkin.Controls.MaterialSingleLineTextField tfPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField tfDescription;
        private MaterialSkin.Controls.MaterialRaisedButton btnSave;
        private FontAwesome.Sharp.IconPictureBox iconPictureBox1;
        private FontAwesome.Sharp.IconPictureBox iconPictureBox2;
        private FontAwesome.Sharp.IconPictureBox iconPictureBox3;
    }
}