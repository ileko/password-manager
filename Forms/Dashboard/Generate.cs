﻿using PasswordGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Manager.Forms
{
    public partial class Generate : Form
    {
        public Generate()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"^[1-9]$|^0[1-9]$|^1[0-9]$|^20$");

            if (string.IsNullOrWhiteSpace(tbPasswordLength.Text))
            {
                MessageBox.Show("Molimo unesite duljinu lozinke!");
                return;
            } else if (regex.IsMatch(tbPasswordLength.Text))
            {
                int length = Int32.Parse(tbPasswordLength.Text);
                var pwd = new Password().IncludeLowercase().IncludeUppercase().IncludeSpecial().LengthRequired(length);
                var result = pwd.Next();

                lblGeneratedPassword.Text = result;
                Console.WriteLine(result);
            }
            else
            {
                MessageBox.Show("Duljina lozinke mora biti broj u rasponu 4-20!");
            }
        }

        private void btnCopyToClipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblGeneratedPassword.Text);
        }
    }
}
