﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Password_Manager
{
    public partial class LoginForm : Form
    {
        private Singleton singleton;

        public LoginForm()
        {
            InitializeComponent();

            this.singleton = Singleton.Instance;

            var pos = this.PointToScreen(pictureBox5.Location);
            pos = pictureBox3.PointToClient(pos);
            pictureBox5.Parent = pictureBox3;
            pictureBox5.Location = pos;
            pictureBox5.BackColor = Color.Transparent;

            var pos2 = this.PointToScreen(pictureBox6.Location);
            pos2 = pictureBox7.PointToClient(pos2);
            pictureBox6.Parent = pictureBox7;
            pictureBox6.Location = pos2;
            pictureBox6.BackColor = Color.Transparent;
        }

        // getting data for moving window feature
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int IParam);

        // handling moving window feature
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        // check user inputs and log in
        public void validate()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tBUsername.Text))
                {
                    MessageBox.Show("Molimo unesite username!");
                    return;
                }

                if (string.IsNullOrWhiteSpace(tBPassword.Text))
                {
                    MessageBox.Show("Molimo unesite password!");
                    return;
                }

                var id = singleton.validation(tBUsername.Text, tBPassword.Text);

                if (id == 0)
                {
                    MessageBox.Show("Neispravni login podaci!");
                    return;
                }

                DashboardForm form1 = new DashboardForm(id, tBPassword.Text);

                form1.Show();
                this.Hide();
            }
            catch (LoginFailed e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        // exit label color
        private void label2_MouseHover(object sender, EventArgs e)
        {
            label2.ForeColor = Color.DarkRed;
        }

        // exit label color
        private void label2_MouseLeave(object sender, EventArgs e)
        {
            label2.ForeColor = Color.Black;
        }

        private void tBUsername_Click(object sender, EventArgs e)
        {
            tBUsername.Clear();
        }

        private void tBPassword_Click(object sender, EventArgs e)
        {
            tBPassword.Clear();
            tBPassword.PasswordChar = '●';
            tBPassword.Font = new System.Drawing.Font(tBPassword.Font.Name, 8F);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            validate();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tBPassword_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            tBPassword.PasswordChar = '●';
            tBPassword.Font = new System.Drawing.Font(tBPassword.Font.Name, 8F);
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            tBUsername.Text = "ileko@test.com";
            tBPassword.Text = "12345";
        }
    }
}
