﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Password_Manager
{
    partial class LoginFailed : Exception
    {
        public LoginFailed() : base("Login function is not working properly.") { }
        public LoginFailed(string msg) : base(msg) { }
    }

    partial class WritingToFileFailed : Exception
    {
        public WritingToFileFailed() : base("Login function is not working properly.") { }
        public WritingToFileFailed(string msg) : base(msg) { }
    }

}
